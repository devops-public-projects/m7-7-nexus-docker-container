
# Docker Container for Nexus
In this project we will deploy Nexus Artifact Repository Manager in as a Docker container.

## Technologies Used
- Docker
- Nexus Artifact Repository Manager
- Node.js
- MongoDB
- Mongo-Express
- DigitalOcean Droplet
- Git
- Linux (Ubuntu)

## Project Description
- Create and Configure a Droplet
- Set up and run Nexus as a Docker container

## Prerequisites
- Digital Ocean account
- Linux machine with Git, Node.js, access configured for remote Nexus server

## Guide Steps

### Create and Configure a Droplet
#### Create Droplet
- Login to your Digital Ocean account
- Droplets > **Create Droplet**
	- Region: Closet to you
	- Basic Plan > Regular > $48/month
		- 8 GB Ram, 4 CPUs, 160 0GB SSD, 5 TB Transfer
	- **Create Droplet**
- Rename as necessary for ease of use, I'll use `docker-nexus`

#### Configure Firewall
- Networking > Firewalls > firewall_name > Droplets > **Add Droplets**
- Enter the new droplet name `docker-nexus`
- **Add Droplet**

### Configure Docker on Remote Server
From your local machine SSH into it and we can start configuring Docker to pull all necessary images and start any necessary containers to run Nexus.
#### Local Machine
- Access remote server
	- `ssh root@DROPLET_IP`
- Accept the fingerprint
	- `yes`

#### Remote Server
- `apt update`
- `snap install docker`
	- If you run into a problem regarding "snap docker assumes unsupported features", you can usually fix this by first running `snap refresh` then the install command.
- [We'll use the official Sonatype Docker Image](https://hub.docker.com/r/sonatype/nexus3)
- Configure Data Persistance
	- `docker volume create --name nexus-data`
- Run Nexus
	- `docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3`

![Successful Nexus Container Start](/images/m7-7-successful-docker-run.png)

![Nexus is listening for requests](/images/m7-7-nexus-listening.png)

- You can access Nexus from your web browser at DROPLET_IP:8081

![Nexus Website Accessible](/images/m7-7-nexus-website-accessible.png)